#ifndef LUCKMERGE_UTILS
#define LUCKMERGE_UTILS

namespace luckmerge {

class starts_with {
 public:
  starts_with(std::string prefix) : prefix_(prefix) {}
  bool operator()(std::string const& str) const {
    return str.compare(0, prefix_.size(), prefix_) == 0;
  }
 private:
  std::string const prefix_;
};

};

#endif
