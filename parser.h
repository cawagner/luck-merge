#ifndef LUCKMERGE_PARSER
#define LUCKMERGE_PARSER

#include <memory>
#include <ios>
#include <sstream>
#include <string>
#include <vector>

namespace luckmerge {

struct parser_mode {
  virtual parser_mode* operator()(std::string const& line, std::ostream& out) = 0;
};

struct no_conflict : public parser_mode {
  virtual parser_mode* operator()(std::string const& line, std::ostream& out);
};

struct in_conflict : public parser_mode {
 public:
  in_conflict();
  virtual parser_mode* operator()(std::string const& line, std::ostream& out);
 private:
  std::vector<std::unique_ptr<std::stringstream>> goshdarn_;
};

}

#endif
