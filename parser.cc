#include <iostream>
#include <random>
#include "parser.h"
#include "utils.h"

namespace luckmerge {

namespace {
  starts_with const starts_conflict("<<<<<<<");
  starts_with const mid_conflict   ("=======");
  starts_with const common_ancestor("|||||||");
  starts_with const ends_conflict  (">>>>>>>");

  std::mt19937 rng(time(0));
}

parser_mode* no_conflict::operator()(std::string const& line, std::ostream& out) {
  if (starts_conflict(line)) {
    return new in_conflict();
  }
  out << line << std::endl;
  return this;
}

in_conflict::in_conflict() : goshdarn_() {
  goshdarn_.push_back(std::unique_ptr<std::stringstream>(new std::stringstream()));
}

parser_mode* in_conflict::operator()(std::string const& line, std::ostream& out) {
  if (mid_conflict(line) || common_ancestor(line)) {
    goshdarn_.push_back(std::unique_ptr<std::stringstream>(new std::stringstream()));
  } else if (ends_conflict(line)) {
    std::size_t const index =
      std::uniform_int_distribution<std::size_t>(0, goshdarn_.size() - 1)(rng);
    out << goshdarn_[index]->str() << std::endl;
    return new no_conflict();
  } else {
    (*goshdarn_.back()) << line << std::endl;
  }
  return this;
}

};