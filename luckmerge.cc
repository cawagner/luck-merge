#include <cstdio>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <memory>
#include <string>

#include "parser.h"
#include "utils.h"

using namespace luckmerge;

// args are MERGED LOCAL BASE REMOTE but we ignore all but MERGED
int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Must provide exactly 1 file to 'resolve' conflicts in" << std::endl;
    return -1;
  }

  std::string tmp_file = std::string(argv[1]) + ".tmp~";

  try {
    std::ifstream src(argv[1]);
    std::ofstream dest(tmp_file);
    std::noskipws(src);

    std::string line;

    parser_mode* luck = new no_conflict();
    while (std::getline(src, line)) {
      parser_mode* const new_luck = (*luck)(line, dest);
      if (luck != new_luck) {
        delete luck;
        luck = new_luck;
      }
    }
  } catch (...) {
    std::cerr << "Aye, your father's mustache" << std::endl;
    return -1;
  }

  rename(tmp_file.c_str(), argv[1]);
  std::cout << "Resolved conflicts!" << std::endl;
}